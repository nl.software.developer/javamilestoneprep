package com.oreillyauto.solutions.geometry;

public class Geometry implements Triangle, Square{
    public Geometry() {
        
    }

    @Override
    public int area(int length) {
        return length *length;
    }

    @Override
    public int area(int base, int height) {
        return (base*height)/2;
    }

}
