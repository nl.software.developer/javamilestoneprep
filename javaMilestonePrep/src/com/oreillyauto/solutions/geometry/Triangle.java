package com.oreillyauto.solutions.geometry;

public interface Triangle {
    public int area(int base, int height); 
}