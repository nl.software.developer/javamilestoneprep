package com.oreillyauto.problems.volume;

public class VolumeImplementation extends Volume {
    int gallonsTotal = 128;

    public VolumeImplementation() {
        // TODO Auto-generated constructor stub
        
    }

    @Override
    void setGallonsAndOunces(int gallons, float ounces) {
        // TODO Auto-generated method stub
        this.gallons = gallons;
        this.ounces = ounces;
        
    }

    @Override
    int getGallons() {
        // TODO Auto-generated method stub
        return gallons;
    }

    @Override
    float getOunces() {
        // TODO Auto-generated method stub
        return ounces;
    }

    @Override
    String getVolumeComparison(Volume volume) {
        // TODO Auto-generated method stub
        float thisDist = this.gallons * gallonsTotal + this.ounces;
        float thatDist = volume.getGallons() * gallonsTotal + volume.getOunces();
        
        if (thisDist > thatDist) {
            return ("First volume is larger.");
        } else if (thisDist < thatDist) {
            return ("Second volume is greater.");
        } else {
            return ("Both volumes are equal.");
        }
    }
        
       
}

