package com.oreillyauto.problems.volume;

/**
 * You are given an abstract class (Volume) with its fields and methods 
 * declared. The object will contain gallons and ounces. Implement the 
 * methods as described (in a class called VolumeImplementation) to 
 * initialize the class instances and return which of the distances is greater, 
 * if there is one. 
 * 
 * Uncomment the main method and ensure that it runs. 
 * The expected output is:
 *    Second volume is greater.
 * 
 * @author jbrannon5
 *
 */
public class TestHarness {
    public static void main(String[] args) {
        Volume vol1 = new VolumeImplementation();
        Volume vol2 = new VolumeImplementation();
        
        int gallons1 = 1;
        float ounces1 = 32;
        
        int gallons2 = 1;
        float ounces2 = 64;
        
        vol1.setGallonsAndOunces(gallons1, ounces1);
        vol2.setGallonsAndOunces(gallons2, ounces2);
        
     
        System.out.println(vol1.getVolumeComparison(vol2));
    }
}
