package com.oreillyauto.problems.geometry;

import java.util.Arrays;
import java.util.Comparator;


/**
 * Build a Geometry class that implements the interfaces:
 *  Square
 *  Triangle
 *  
 * Use one half the base times the height to calculate the 
 * area of a triangle. Use length times width to calculate
 * the area of a square.
 * 
 * Uncomment the main method once you have created the 
 * Geometry class.
 * 
 * EXPEXTED OUTPUT:
 * I implemented: 
 * Square
 * Triangle
 * Triangle area = 6
 * Square area = 36
 * 
 * @author jbrannon5
 *
 */
public class TestHarness {
    
    public static void main(String[] args) {
        Geometry g = new Geometry();
        System.out.println("I implemented: ");
        implementedInterfaceNames(g);
        int base = 2;
        int height = 6;
        System.out.println("Triangle area = " + g.area(base, height));
        System.out.println("Square area = " + g.area(height));
    }

    static void implementedInterfaceNames(Object o) {
        Class[] interfaces = o.getClass().getInterfaces();
        
        Arrays.sort(interfaces, new Comparator<Class>() {
            public int compare(Class o1, Class o2) {
                return o1.getSimpleName().compareTo(o2.getSimpleName());
            }
        });
        
        for (Class c : interfaces) {
            System.out.println(c.getSimpleName());
        }
    }

}
